# MoEE

**Demo Application**

Technologies Used - 
    Java 8, Spring Boot, JWT, Liquibase , swagger 2
    
**Property File**
Change below mentioned fields with your values

server.port:{your server port}
spring.datasource.url={db url}
spring.jpa.properties.hibernate.default_schema={default schema}
spring.datasource.username= {user name}
spring.datasource.password= {password}
spring.liquibase.default-schema={default schema}

Make sure to create an empty schema , liquibase will enter tables and master data

**Build project with** 

mvn clean install

collect jar file in target directory and run it with 

java -jar {jar file name}

application will start and you can access it by

http://localhost:/{your server port}/swagger-ui.html

**Default users details**

username - lak , password - admin
username - din , password - password

you will get a jwt token once successfully logged in. 

Use token to authenticate via swagger ui.

Thank you.


 

