package com.monese.bank.demo.service;

import com.monese.bank.demo.dto.AccountBalanceDto;
import com.monese.bank.demo.model.Account;
import com.monese.bank.demo.repository.AccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @MockBean
    AccountRepository accountRepository;

    @Autowired
    AccountService accountService;

    @Test
    public void getAccountBalanceTest(){
        Account account = new Account(1L,200);
        Mockito.when(accountRepository.findById(1L)).thenReturn(java.util.Optional.of(account));
        AccountBalanceDto accountBalanceDto = accountService.getAccountBalance(1L);
        Assert.assertEquals(200f, accountBalanceDto.getBalance(), 0.00);

    }
}
