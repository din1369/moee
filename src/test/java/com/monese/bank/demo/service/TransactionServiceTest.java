package com.monese.bank.demo.service;

import com.monese.bank.demo.dto.DepositDto;
import com.monese.bank.demo.dto.TransactionDto;
import com.monese.bank.demo.dto.TransferDto;
import com.monese.bank.demo.model.Account;
import com.monese.bank.demo.model.User;
import com.monese.bank.demo.repository.AccountRepository;
import com.monese.bank.demo.repository.TransactionRepository;
import com.monese.bank.demo.repository.UserRepository;
import com.monese.bank.demo.util.ResponseWrapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTest {

    @MockBean
    UserRepository userRepository;

    @MockBean
    AccountRepository accountRepository;

    @MockBean
    TransactionRepository transactionRepository;

    @Autowired
    TransactionService transactionService;

    @Test
    public void depositToAccountTest(){

        Account account = new Account(1L,200);
        DepositDto depositDto = new DepositDto(1L,400);
        User user = new User(1L);
        Mockito.when(accountRepository.findById(1L)).thenReturn(java.util.Optional.of(account));
        Mockito.when(userRepository.findByUsername("lak")).thenReturn(user);

        TransactionDto transactionDto = transactionService.depositToAccount(depositDto,"lak");
        Assert.assertEquals(600f, transactionDto.getBalance(), 0.00);
        Assert.assertEquals(400f, transactionDto.getChangedAmount(), 0.00);

    }

    @Test
    public void transferToAccountTest(){

        Account fromAccount = new Account(1L,1000);
        Account toAccount = new Account(2L,200);
        TransferDto transferDto = new TransferDto(1L,2L,400);
        User user = new User(1L);

        Mockito.when(accountRepository.findById(1L)).thenReturn(java.util.Optional.of(fromAccount));
        Mockito.when(accountRepository.findById(2L)).thenReturn(java.util.Optional.of(toAccount));
        Mockito.when(userRepository.findByUsername("lak")).thenReturn(user);

        ResponseWrapper responseWrapper = transactionService.moneyTransfer(transferDto,"lak");
        TransactionDto transactionDto = (TransactionDto) responseWrapper.getBody();
        Assert.assertEquals(600f, transactionDto.getBalance(), 0.00);
        Assert.assertEquals(400f, transactionDto.getChangedAmount(), 0.00);
        Assert.assertEquals("SUCCESS",responseWrapper.getStatus().toString());

    }
}
