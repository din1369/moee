package com.monese.bank.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "account")
public class Account extends MoneseBaseModel {

    public Account() {
    }

    public Account(Long id,float balance) {
        this.id = id;
        this.balance = balance;
    }

    @Column(name = "account_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Column(name = "branch", nullable = false)
    private String branch;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "balance")
    private float balance;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<Transactions> transactions;

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }
}
