package com.monese.bank.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "transactions")
public class Transactions extends MoneseBaseModel {

    @Column(name = "balance", nullable = false)
    private float balance;

    @Column(name = "changed_amount", nullable = false)
    private float changedAmount;

    @Column(name = "send_to")
    private Long  sendToId;

    @Column(name = "received_from")
    private Long  receivedFromId;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getChangedAmount() {
        return changedAmount;
    }

    public void setChangedAmount(float changedAmount) {
        this.changedAmount = changedAmount;
    }

    public Long getSendToId() {
        return sendToId;
    }

    public void setSendToId(Long sendToId) {
        this.sendToId = sendToId;
    }

    public Long getReceivedFromId() {
        return receivedFromId;
    }

    public void setReceivedFromId(Long receivedFromId) {
        this.receivedFromId = receivedFromId;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
