package com.monese.bank.demo.model;

public enum TransactionType {
    DEPOSIT,TRANSFER
}
