package com.monese.bank.demo.model;

public enum TransactionStatus {
    SUCCESS, FAILED, ROLLBACK
}
