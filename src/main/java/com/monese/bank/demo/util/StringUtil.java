package com.monese.bank.demo.util;

import com.google.common.base.Preconditions;
import com.monese.bank.demo.exception.RequiredRequestDataNotFoundException;
import org.springframework.util.StringUtils;

public class StringUtil {

    private StringUtil() {
    }

    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static <T> void validateNotNullOREmpty(final T reference, final String errorMessage) {
        try {
            Preconditions.checkNotNull(reference, errorMessage);
            if(StringUtils.isEmpty(String.valueOf(reference))) {
                throw new IllegalArgumentException(errorMessage);
            }
        } catch (NullPointerException | IllegalArgumentException validateArgumentError) {
            throw new RequiredRequestDataNotFoundException(validateArgumentError.getMessage());
        }
    }
}
