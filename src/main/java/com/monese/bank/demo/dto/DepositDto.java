package com.monese.bank.demo.dto;

public class DepositDto implements BaseDto{

    Long accountId;
    float amount;

    public DepositDto() {
    }

    public DepositDto(Long accountId, float amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
