package com.monese.bank.demo.dto;

public class AccountBalanceDto implements BaseDto{

    Long accountId;
    float balance;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
