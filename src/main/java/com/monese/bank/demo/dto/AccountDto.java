package com.monese.bank.demo.dto;

import com.monese.bank.demo.model.AccountType;

public class AccountDto implements BaseDto{

    Long accountId;
    AccountType accountType;
    String branch;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
