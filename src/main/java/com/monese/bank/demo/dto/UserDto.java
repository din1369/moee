package com.monese.bank.demo.dto;

import java.util.List;

public class UserDto implements BaseDto{

    Long userId;
    String userName;
    String email;
    String address;
    String contactNumber;
    List<AccountStatementDto> accountStatementDtos;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public List<AccountStatementDto> getAccountStatementDtos() {
        return accountStatementDtos;
    }

    public void setAccountStatementDtos(List<AccountStatementDto> accountStatementDtos) {
        this.accountStatementDtos = accountStatementDtos;
    }
}
