package com.monese.bank.demo.dto;

import com.monese.bank.demo.model.TransactionStatus;
import com.monese.bank.demo.model.TransactionType;

import java.util.Date;

public class TransactionDto implements BaseDto{

    Long transactionId;
    Date createdDate;
    Long createdUser;
    float balance;
    float changedAmount;
    Long  sendToId;
    Long receivedFromId;
    TransactionStatus status;
    TransactionType transactionType;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getChangedAmount() {
        return changedAmount;
    }

    public void setChangedAmount(float changedAmount) {
        this.changedAmount = changedAmount;
    }

    public Long getSendToId() {
        return sendToId;
    }

    public void setSendToId(Long sendToId) {
        this.sendToId = sendToId;
    }

    public Long getReceivedFromId() {
        return receivedFromId;
    }

    public void setReceivedFromId(Long receivedFromId) {
        this.receivedFromId = receivedFromId;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
