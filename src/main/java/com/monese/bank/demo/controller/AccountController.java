package com.monese.bank.demo.controller;

import com.monese.bank.demo.dto.AccountBalanceDto;
import com.monese.bank.demo.dto.AccountDto;
import com.monese.bank.demo.dto.AccountStatementDto;
import com.monese.bank.demo.service.AccountService;
import com.monese.bank.demo.util.jwt.JwtTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/account")
@Api(value = "account", tags = "Account Controller")
public class AccountController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    AccountService accountService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping(method = RequestMethod.POST, value = "/create",produces={"application/json"},consumes = {"application/json"})
    @ApiOperation(value = "Create Bank Account", notes = "Used to create account",response = AccountDto.class )
    @ApiResponse(code = 200, message = "Account Id", response = AccountDto.class)
    @ResponseBody
    public ResponseEntity createAccount(@RequestBody AccountDto accountDto, HttpServletRequest request) {

        String token = request.getHeader(tokenHeader);
        String userName = jwtTokenUtil.getUsernameFromToken(token);
        return ResponseEntity.ok(accountService.createAccount(accountDto,userName));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/balance/{accountId}",produces={"application/json"})
    @ApiOperation(value = "Get balance details of a account",response = AccountBalanceDto.class )
    @ApiResponse(code = 200, message = "Get balance details", response = AccountBalanceDto.class)
    @ResponseBody
    public ResponseEntity getAccountBalance(@PathVariable("accountId") Long accountId) {

        return ResponseEntity.ok(accountService.getAccountBalance(accountId));
    }


    @RequestMapping(method = RequestMethod.GET, value = "/statement/{accountId}",produces={"application/json"})
    @ApiOperation(value = "Get statement details of a account",response = AccountStatementDto.class )
    @ApiResponse(code = 200, message = "Get statement details", response = AccountStatementDto.class)
    @ResponseBody
    public ResponseEntity getAccountStatement(@PathVariable("accountId") Long accountId) {

        return ResponseEntity.ok(accountService.getAccountStatement(accountId));
    }



}
