package com.monese.bank.demo.controller;

import com.monese.bank.demo.dto.DepositDto;
import com.monese.bank.demo.dto.TransactionDto;
import com.monese.bank.demo.dto.TransferDto;
import com.monese.bank.demo.service.TransactionService;
import com.monese.bank.demo.util.jwt.JwtTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/transaction")
@Api(value = "transaction", tags = "Transaction Controller")
public class TransactionController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    TransactionService transactionService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping(method = RequestMethod.POST, value = "/deposit",produces={"application/json"},consumes = {"application/json"})
    @ApiOperation(value = "Deposit To Bank Account", notes = "Used to deposit money",response = TransactionDto.class )
    @ApiResponse(code = 200, message = "Success with transaction id", response = TransactionDto.class)
    @ResponseBody
    public ResponseEntity depositToAccount(@RequestBody DepositDto depositDto, HttpServletRequest request) {

        String token = request.getHeader(tokenHeader);
        String userName = jwtTokenUtil.getUsernameFromToken(token);
        return ResponseEntity.ok(transactionService.depositToAccount(depositDto,userName));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/transfer",produces={"application/json"},consumes = {"application/json"})
    @ApiOperation(value = "Transfer Money", notes = "Used to transfer money between accounts",response = TransactionDto.class )
    @ApiResponse(code = 200, message = "Success with TransactionDto", response = TransactionDto.class)
    @ResponseBody
    public ResponseEntity moneyTransfer(@RequestBody TransferDto transferDto, HttpServletRequest request) {

        String token = request.getHeader(tokenHeader);
        String userName = jwtTokenUtil.getUsernameFromToken(token);
        return ResponseEntity.ok(transactionService.moneyTransfer(transferDto,userName));
    }
}
