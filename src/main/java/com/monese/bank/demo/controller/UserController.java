package com.monese.bank.demo.controller;

import com.monese.bank.demo.dto.UserDto;
import com.monese.bank.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Api(value = "user", tags = "User Controller")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/detailStatement/{userId}",produces={"application/json"})
    @ApiOperation(value = "Get all the user details with account statement",response = UserDto.class )
    @ApiResponse(code = 200, message = "Get all details", response = UserDto.class)
    @ResponseBody
    public ResponseEntity getUserDetailedStatement(@PathVariable("userId") Long userId) {

        return ResponseEntity.ok(userService.getUserDetailedStatement(userId));
    }
}
