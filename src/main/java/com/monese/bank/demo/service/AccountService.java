package com.monese.bank.demo.service;

import com.monese.bank.demo.dto.AccountBalanceDto;
import com.monese.bank.demo.dto.AccountDto;
import com.monese.bank.demo.dto.AccountStatementDto;

public interface AccountService {

    AccountDto createAccount(AccountDto accountDto, String userName);

    AccountBalanceDto getAccountBalance(Long accountId);

    AccountStatementDto getAccountStatement(Long accountId);
}
