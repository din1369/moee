package com.monese.bank.demo.service;

import com.monese.bank.demo.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDetails loadUserByUsername(String username);

    UserDto getUserDetailedStatement(Long userId);
}
