package com.monese.bank.demo.service.impl;

import com.monese.bank.demo.dto.AccountBalanceDto;
import com.monese.bank.demo.dto.AccountDto;
import com.monese.bank.demo.dto.AccountStatementDto;
import com.monese.bank.demo.dto.TransactionDto;
import com.monese.bank.demo.exception.RequiredRequestDataNotFoundException;
import com.monese.bank.demo.model.Account;
import com.monese.bank.demo.model.Transactions;
import com.monese.bank.demo.model.User;
import com.monese.bank.demo.repository.AccountRepository;
import com.monese.bank.demo.repository.UserRepository;
import com.monese.bank.demo.service.AccountService;
import com.monese.bank.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountRepository accountRepository;


    @Override
    public AccountDto createAccount(AccountDto accountDto, String userName) {
        StringUtil.validateNotNullOREmpty(accountDto.getBranch(), "Account Branch is mandatory");
        User user = userRepository.findByUsername(userName);
        if(user == null){
            throw new RequiredRequestDataNotFoundException("User Not Available");
        }
        //ToDo Create Util for convert dto to models
        Account account = new Account();
        account.setCreatedUserId(user.getId());
        account.setCreatedDate(new Date());
        account.setAccountType(accountDto.getAccountType());
        account.setBranch(accountDto.getBranch());
        account.setUser(user);
        accountRepository.save(account);
        AccountDto rtnAccountDto = new AccountDto();
        rtnAccountDto.setAccountId(account.getId());
        return rtnAccountDto;
    }

    @Override
    public AccountBalanceDto getAccountBalance(Long accountId) {
        StringUtil.validateNotNullOREmpty(accountId, "Account Id is mandatory");
        Optional<Account> account = accountRepository.findById(accountId);
        if(account.isPresent()){
            AccountBalanceDto accountBalanceDto = new AccountBalanceDto();
            accountBalanceDto.setAccountId(accountId);
            accountBalanceDto.setBalance(account.get().getBalance());
            return  accountBalanceDto;
        }else{
            throw new RequiredRequestDataNotFoundException("Account Not Available");
        }
    }

    @Override
    public AccountStatementDto getAccountStatement(Long accountId) {
        StringUtil.validateNotNullOREmpty(accountId, "Account Id is mandatory");
        Optional<Account> account = accountRepository.findById(accountId);
        if(account.isPresent()){
            AccountStatementDto accountStatementDto = new AccountStatementDto();
            accountStatementDto.setAccountId(accountId);
            accountStatementDto.setAccountType(account.get().getAccountType());
            accountStatementDto.setBranch(account.get().getBranch());
            accountStatementDto.setUserName(account.get().getUser().getUsername());
            accountStatementDto.setBalance(account.get().getBalance());
            accountStatementDto.setCreatedDate(account.get().getCreatedDate());
            if(account.get().getUpdatedDate() != null){

                accountStatementDto.setUpdatedDate(account.get().getUpdatedDate());
            }
            List<TransactionDto> transactionDtoList = getTransactions(account.get().getTransactions());
            accountStatementDto.setTransactionDtoList(transactionDtoList);
            return  accountStatementDto;
        }else{
            throw new RequiredRequestDataNotFoundException("Account Not Available");
        }
    }

    private List<TransactionDto> getTransactions(List<Transactions> transactionsList ){
        return transactionsList.stream().map(transactions -> {
            TransactionDto transactionDto = new TransactionDto();
            transactionDto.setTransactionId(transactions.getId());
            transactionDto.setCreatedDate(transactions.getCreatedDate());
            transactionDto.setCreatedUser(transactions.getCreatedUserId());
            transactionDto.setBalance(transactions.getBalance());
            transactionDto.setChangedAmount(transactions.getChangedAmount());
            if(transactions.getSendToId()!=null){
                transactionDto.setSendToId(transactions.getSendToId());}
            if(transactions.getReceivedFromId()!=null){
                transactionDto.setReceivedFromId(transactions.getReceivedFromId());}
            transactionDto.setStatus(transactions.getStatus());
            transactionDto.setTransactionType(transactions.getTransactionType());
            return transactionDto;
        }).collect(Collectors.toList());
    }
}
