package com.monese.bank.demo.service;

import com.monese.bank.demo.dto.DepositDto;
import com.monese.bank.demo.dto.TransactionDto;
import com.monese.bank.demo.dto.TransferDto;
import com.monese.bank.demo.util.ResponseWrapper;

public interface TransactionService {

    TransactionDto depositToAccount(DepositDto depositeDto, String userName);

    ResponseWrapper moneyTransfer(TransferDto transferDto, String userName);
}
