package com.monese.bank.demo.service.impl;

import com.monese.bank.demo.dto.AccountStatementDto;
import com.monese.bank.demo.dto.UserDto;
import com.monese.bank.demo.exception.RequiredRequestDataNotFoundException;
import com.monese.bank.demo.model.Account;
import com.monese.bank.demo.model.User;
import com.monese.bank.demo.repository.UserRepository;
import com.monese.bank.demo.service.AccountService;
import com.monese.bank.demo.service.UserService;
import com.monese.bank.demo.util.StringUtil;
import com.monese.bank.demo.util.jwt.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return new JwtUser(user);
    }

    @Override
    public UserDto getUserDetailedStatement(Long userId) {
        StringUtil.validateNotNullOREmpty(userId, "User Id is mandatory");
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            UserDto userDto = new UserDto();
            userDto.setUserId(userId);
            userDto.setUserName(user.get().getUsername());
            userDto.setAddress(user.get().getAddress());
            userDto.setContactNumber(user.get().getContactNumber());
            userDto.setEmail(user.get().getEmail());

            List<Account> accounts = user.get().getAccounts();
            List<AccountStatementDto> accountStatementDtos = accounts.stream().map(account -> {
                AccountStatementDto accountStatementDto = accountService.getAccountStatement(account.getId());
                return accountStatementDto;
            }).collect(Collectors.toList());
            userDto.setAccountStatementDtos(accountStatementDtos);
            return userDto;
        } else {
            throw new RequiredRequestDataNotFoundException("User Not Available");
        }
    }
}
