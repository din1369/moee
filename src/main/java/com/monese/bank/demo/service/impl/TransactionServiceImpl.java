package com.monese.bank.demo.service.impl;

import com.monese.bank.demo.dto.DepositDto;
import com.monese.bank.demo.dto.TransactionDto;
import com.monese.bank.demo.dto.TransferDto;
import com.monese.bank.demo.exception.RequiredRequestDataNotFoundException;
import com.monese.bank.demo.model.*;
import com.monese.bank.demo.repository.AccountRepository;
import com.monese.bank.demo.repository.TransactionRepository;
import com.monese.bank.demo.repository.UserRepository;
import com.monese.bank.demo.service.TransactionService;
import com.monese.bank.demo.util.ResponseWrapper;
import com.monese.bank.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Transactional
@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public TransactionDto depositToAccount(DepositDto depositDto, String userName) {
        StringUtil.validateNotNullOREmpty(depositDto.getAccountId(), "Account Id is mandatory");
        StringUtil.validateNotNullOREmpty(depositDto.getAmount(), "Amount is mandatory");

        User user = userRepository.findByUsername(userName);
        Optional<Account> optionalAccount = accountRepository.findById(depositDto.getAccountId());
        if (user == null || !optionalAccount.isPresent()) {
            throw new RequiredRequestDataNotFoundException("User Or Account Not Available");
        }

        if(depositDto.getAmount()>0){
            Account account = optionalAccount.get();
            account.setUpdatedDate( new Date());
            account.setEditedUserId(user.getId());
            account.setBalance(account.getBalance()+depositDto.getAmount());
            //ToDo Create Util for convert dto to models
            Transactions transactions = getTransactions(depositDto.getAmount(), user, account,TransactionType.DEPOSIT);

            accountRepository.save(account);
            transactionRepository.save(transactions);
            TransactionDto rtnTransactionDto = new TransactionDto();
            rtnTransactionDto.setTransactionId(transactions.getId());
            rtnTransactionDto.setStatus(TransactionStatus.SUCCESS);
            rtnTransactionDto.setBalance(transactions.getBalance());
            rtnTransactionDto.setChangedAmount(depositDto.getAmount());
            return  rtnTransactionDto;

        }else{
            throw new RequiredRequestDataNotFoundException("Deposit amount should more than 0");
        }
    }

    @Override
    public ResponseWrapper moneyTransfer(TransferDto transferDto, String userName) {
        StringUtil.validateNotNullOREmpty(transferDto.getFromAccountId(), "From Account Id is mandatory");
        StringUtil.validateNotNullOREmpty(transferDto.getToAccountId(), "To Account Id is mandatory");
        StringUtil.validateNotNullOREmpty(transferDto.getAmount(), "Amount is mandatory");
        User user = userRepository.findByUsername(userName);
        Optional<Account> OptionalFromAccount = accountRepository.findById(transferDto.getFromAccountId());
        Optional<Account> OptionalToAccount = accountRepository.findById(transferDto.getToAccountId());
        if (user == null || !OptionalFromAccount.isPresent() || !OptionalToAccount.isPresent()) {
            throw new RequiredRequestDataNotFoundException("User ,From Account or To Account Not Available");
        }
        Account fromAccount = OptionalFromAccount.get();
        Account toAccount = OptionalToAccount.get();
        if(checkAccountHaveEnoughBalanceAndPositive(fromAccount.getBalance(),transferDto.getAmount())){

            Transactions fromAccountTransactions = updateFromAccount(transferDto, user, fromAccount);
            updateToAccount(transferDto, user, toAccount);

            TransactionDto rtnTransactionDto = new TransactionDto();
            rtnTransactionDto.setTransactionId(fromAccountTransactions.getId());
            rtnTransactionDto.setStatus(TransactionStatus.SUCCESS);
            rtnTransactionDto.setBalance(fromAccountTransactions.getBalance());
            rtnTransactionDto.setChangedAmount(transferDto.getAmount());
            return  ResponseWrapper.success(rtnTransactionDto);

        }else{
           return ResponseWrapper.failWithMessage(HttpStatus.BAD_REQUEST.toString(),"Account Balance is not enough or " +
                    "transfer amount must be positive");
        }
    }

    private void updateToAccount(TransferDto transferDto, User user, Account toAccount) {
        toAccount.setUpdatedDate( new Date());
        toAccount.setEditedUserId(user.getId());
        toAccount.setBalance(toAccount.getBalance() + transferDto.getAmount());
        Transactions toAccountTransactions = getTransactions(transferDto.getAmount(), user, toAccount, TransactionType.TRANSFER);
        accountRepository.save(toAccount);
        transactionRepository.save(toAccountTransactions);
    }

    private Transactions updateFromAccount(TransferDto transferDto, User user, Account fromAccount) {
        fromAccount.setUpdatedDate( new Date());
        fromAccount.setEditedUserId(user.getId());
        fromAccount.setBalance(fromAccount.getBalance() - transferDto.getAmount());
        Transactions fromAccountTransactions = getTransactions(transferDto.getAmount(), user, fromAccount, TransactionType.TRANSFER);
        accountRepository.save(fromAccount);
        transactionRepository.save(fromAccountTransactions);
        return fromAccountTransactions;
    }

    private Transactions getTransactions(float amount, User user, Account account, TransactionType transactionType) {
        Transactions transactions = new Transactions();
        transactions.setCreatedDate(new Date());
        transactions.setCreatedUserId(user.getId());
        transactions.setAccount(account);
        transactions.setTransactionType(transactionType);
        transactions.setBalance(account.getBalance());
        transactions.setChangedAmount(amount);
        transactions.setStatus(TransactionStatus.SUCCESS);
        return transactions;
    }

    private boolean checkAccountHaveEnoughBalanceAndPositive(float fromBalance, float amount){
        return fromBalance>amount && amount>0;
    }
}
