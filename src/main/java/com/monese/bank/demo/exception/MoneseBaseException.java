package com.monese.bank.demo.exception;

public class MoneseBaseException extends RuntimeException{

    private String errorCode;

    public MoneseBaseException() {
    }

    public MoneseBaseException(String message) {
        super(message);
    }

    public MoneseBaseException(Throwable cause) {
        super(cause);
    }

    public MoneseBaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoneseBaseException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
