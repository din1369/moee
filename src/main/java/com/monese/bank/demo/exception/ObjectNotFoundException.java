package com.monese.bank.demo.exception;

public class ObjectNotFoundException extends MoneseBaseException {
    public ObjectNotFoundException(String message) {
        super(message);
    }
}
